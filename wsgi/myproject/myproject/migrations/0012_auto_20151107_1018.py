# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('SampleApp', '0011_auto_20151107_1010'),
    ]

    operations = [
        migrations.RenameField(
            model_name='unifamily',
            old_name='familyname',
            new_name='famname',
        ),
    ]
