# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('SampleApp', '0007_auto_20151107_0950'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='unifamily',
            name='username',
        ),
        migrations.AddField(
            model_name='unifamily',
            name='user',
            field=models.CharField(default=datetime.datetime(2015, 11, 7, 4, 14, 4, 756000, tzinfo=utc), max_length=100),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='unifamily',
            name='uname',
            field=models.CharField(unique=True, max_length=100),
        ),
    ]
