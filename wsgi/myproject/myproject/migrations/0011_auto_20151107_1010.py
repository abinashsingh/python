# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('SampleApp', '0010_auto_20151107_1005'),
    ]

    operations = [
        migrations.AlterField(
            model_name='unifamily',
            name='uqname',
            field=models.CharField(max_length=100),
        ),
    ]
