# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('SampleApp', '0014_auto_20151107_1242'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='person',
            name='password',
        ),
        migrations.AddField(
            model_name='person',
            name='address',
            field=models.CharField(default=datetime.datetime(2015, 11, 7, 13, 15, 33, 260000, tzinfo=utc), max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='person',
            name='age',
            field=models.CharField(default=datetime.datetime(2015, 11, 7, 13, 16, 1, 229000, tzinfo=utc), max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='person',
            name='birthdate',
            field=models.CharField(default=datetime.datetime(2015, 11, 7, 13, 16, 57, 346000, tzinfo=utc), max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='person',
            name='caste',
            field=models.CharField(default=datetime.datetime(2015, 11, 7, 13, 17, 9, 550000, tzinfo=utc), max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='person',
            name='contact',
            field=models.CharField(default=datetime.datetime(2015, 11, 7, 13, 17, 20, 647000, tzinfo=utc), max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='person',
            name='deathdate',
            field=models.CharField(default=datetime.datetime(2015, 11, 7, 13, 17, 31, 539000, tzinfo=utc), max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='person',
            name='education',
            field=models.CharField(default=datetime.datetime(2015, 11, 7, 13, 18, 50, 821000, tzinfo=utc), max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='person',
            name='membername',
            field=models.CharField(default=datetime.datetime(2015, 11, 7, 13, 18, 58, 503000, tzinfo=utc), max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='person',
            name='nationality',
            field=models.CharField(default=datetime.datetime(2015, 11, 7, 13, 19, 5, 652000, tzinfo=utc), max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='person',
            name='occupation',
            field=models.CharField(default=datetime.datetime(2015, 11, 7, 13, 19, 16, 618000, tzinfo=utc), max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='person',
            name='relation',
            field=models.CharField(default=datetime.datetime(2015, 11, 7, 13, 19, 33, 370000, tzinfo=utc), max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='person',
            name='religion',
            field=models.CharField(default=datetime.datetime(2015, 11, 7, 13, 19, 43, 440000, tzinfo=utc), max_length=100),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='person',
            name='username',
            field=models.CharField(max_length=100),
        ),
    ]
