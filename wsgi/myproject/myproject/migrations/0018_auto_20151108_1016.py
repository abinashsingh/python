# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('SampleApp', '0017_auto_20151108_1009'),
    ]

    operations = [
        migrations.AlterField(
            model_name='person',
            name='uniquename',
            field=models.CharField(max_length=100),
        ),
    ]
