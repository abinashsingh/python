# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('SampleApp', '0006_auto_20151107_0934'),
    ]

    operations = [
        migrations.RenameField(
            model_name='unifamily',
            old_name='uniname',
            new_name='uname',
        ),
    ]
