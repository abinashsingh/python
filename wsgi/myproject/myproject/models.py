#************************************************************************************************************************************
# Name: Abinash Singh Thagunna
# File Name: models.py
# Description: Person, Unifamily are the two class model for database model for geneological tree
#*************************************************************************************************************************************
from django.db import models



class  Person(models.Model):
   username = models.CharField(max_length=100)
   memberfirstname = models.CharField(max_length=100)
   membermiddlename = models.CharField(max_length=100, null=True)
   memberlastname = models.CharField(max_length=100, null=True)
   relation = models.CharField(max_length=100)
   age = models.CharField(max_length=100)
   birthdate = models.CharField(max_length=100)
   deathdate = models.CharField(max_length=100)
   caste = models.CharField(max_length=100)
   religion = models.CharField(max_length=100)
   nationality = models.CharField(max_length=100)
   occupation = models.CharField(max_length=100)
   education = models.CharField(max_length=100)
   address = models.CharField(max_length=100)
   contact = models.CharField(max_length=100)
   familylastname = models.CharField(max_length=100, null=True)
   uniquename = models.CharField(max_length=100)


class Unifamily(models.Model):
   familylastname = models.CharField(max_length=100)
   uniquename = models.CharField(max_length=100, unique=True)
   username = models.CharField(max_length=100)

