#************************************************************************************************************************************
# Name: Abinash Singh Thagunna
# File Name: forms.py
# Description: RegistrationForm, UnifamilyForm,PersonForm are the three classes model for geneological tree form templates
#*************************************************************************************************************************************
from django import forms
import re
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from SampleApp.models import Unifamily


class SignupForm(forms.Form):                        #this class has not been used
   username = forms.CharField(max_length=280)
   password = forms.CharField(max_length=230)


class UnifamilyForm(forms.Form):
   familylastname = forms.CharField(max_length=100)
   uniquename = forms.CharField(max_length=100)
   username = forms.CharField(max_length=100)

   def clean_uniquename(self):
        uniquename = self.cleaned_data['uniquename']
        try:
            Unifamily.objects.get(uniquename=uniquename)
        except ObjectDoesNotExist:
            return uniquename
        raise forms.ValidationError('Tracking Name is already taken.')


class RegistrationForm(forms.Form):                                       #this class is used for user registration
    username = forms.CharField(label='Username', max_length=30)
    email = forms.EmailField(label='Email')
    password1 = forms.CharField(label='Password',
                          widget=forms.PasswordInput())
    password2 = forms.CharField(label='Password (Again)',
                        widget=forms.PasswordInput())
    def clean_password2(self):
        if 'password1' in self.cleaned_data:
            password1 = self.cleaned_data['password1']
            password2 = self.cleaned_data['password2']
            if password1 == password2:
                return password2
        raise forms.ValidationError('Passwords do not match.')
    def clean_username(self):
        username = self.cleaned_data['username']
        if not re.search(r'^\w+$', username):
            raise forms.ValidationError('Username can only contain alphanumeric characters and the underscore.')
        try:
            User.objects.get(username=username)
        except ObjectDoesNotExist:
            return username
        raise forms.ValidationError('Username is already taken.')


class PersonForm(forms.Form):                                  # this class is used to add family member details
   username = forms.CharField(max_length=100)
   memberfirstname = forms.CharField(max_length=100)
   membermiddlename = forms.CharField(max_length=100)
   memberlastname = forms.CharField(max_length=100)
   relation = forms.CharField(max_length=100)
   age = forms.CharField(max_length=100)
   birthdate = forms.CharField(max_length=100)
   deathdate = forms.CharField(max_length=100)
   caste = forms.CharField(max_length=100)
   religion = forms.CharField(max_length=100)
   nationality = forms.CharField(max_length=100)
   occupation = forms.CharField(max_length=100)
   education = forms.CharField(max_length=100)
   address = forms.CharField(max_length=100)
   contact = forms.CharField(max_length=100)
   familylastname = forms.CharField(max_length=100)
   uniquename = forms.CharField(max_length=100)

