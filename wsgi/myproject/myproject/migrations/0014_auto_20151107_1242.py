# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('SampleApp', '0013_auto_20151107_1038'),
    ]

    operations = [
        migrations.RenameField(
            model_name='unifamily',
            old_name='famm',
            new_name='familylastname',
        ),
        migrations.RenameField(
            model_name='unifamily',
            old_name='uqname',
            new_name='username',
        ),
        migrations.RemoveField(
            model_name='unifamily',
            name='user',
        ),
        migrations.AddField(
            model_name='unifamily',
            name='uniquename',
            field=models.CharField(default=datetime.datetime(2015, 11, 7, 6, 57, 18, 151000, tzinfo=utc), unique=True, max_length=100),
            preserve_default=False,
        ),
    ]
