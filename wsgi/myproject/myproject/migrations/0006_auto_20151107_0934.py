# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('SampleApp', '0005_auto_20151107_0922'),
    ]

    operations = [
        migrations.RenameField(
            model_name='unifamily',
            old_name='user',
            new_name='username',
        ),
    ]
