#************************************************************************************************************************************
# Name: Abinash Singh Thagunna
# File Name: views.py
# Description: the below methods are used to redirect page, save data
#*************************************************************************************************************************************

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render, render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template.loader import get_template
from django.template import Context, RequestContext
from SampleApp.forms import RegistrationForm, UnifamilyForm, PersonForm
from SampleApp.models import Person, Unifamily
from django.contrib.auth import logout, login


def hello_template(request):
    t=get_template('register.html')
    html=t.render(Context())
    return HttpResponse(html)


@login_required
def form_template(request):
 data= Person.objects.all()
 track= Unifamily.objects.filter(username=request.user.username)
 return render(request, "mainform.html", {'username':request.user.username, 'data': data, 'track': track})
         #f=get_template('mainform.html')
         #html1=f.render(Context())
        # return HttpResponse(html1)



@login_required
def family_template(request):  #redirects to template to add tracking name for the geneological tree
 return render(request, "addfamilyname.html", {'username':request.user.username})




def fam_template(request):  #used to save Post form data to database
    if request.method == 'GET':
       return render(request, "addfamilyname.html", {'username':request.user.username})
    else:
        # A POST request: Handle Form Upload
        form = UnifamilyForm(request.POST) # Bind data from request.POST into a PostForm

        # If data is valid, proceeds to create a new post and redirect the user
        if form.is_valid():
            username = form.cleaned_data['username']
            uniquename = form.cleaned_data['uniquename']
            familylastname = form.cleaned_data['familylastname']
            unifamily = Unifamily(familylastname=familylastname, uniquename=uniquename, username=username)
            unifamily.save()
            return HttpResponseRedirect('/home/')
        else:
            messages.error(request, "Tracking name already in use or required fileds are left blank.")
            return render(request, "addfamilyname.html", {'form':form})

def register_page(request): #this is used to save data of users for registration
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(username=form.cleaned_data['username'],password=form.cleaned_data['password1'],
                                            email=form.cleaned_data['email'])
            return HttpResponseRedirect('/')
    form = RegistrationForm()
    variables = RequestContext(request, {'form': form})
    return render_to_response('registration/register.html',variables)

def main_page(request): # this method has not been used
    return render_to_response('ok.html', RequestContext(request))

def logout_page(request): # this is used to sign out the logged in user
    logout(request)
    return HttpResponseRedirect('/')


def mem_template(request): #this is used to save data of family member details to database
    if request.method == 'GET':
       data= Person.objects.all()
       track= Unifamily.objects.filter(username=request.user.username)
       return render(request, "mainform.html", {'username':request.user.username, 'data': data, 'track': track})
    else:
        # A POST request: Handle Form Upload
        form = PersonForm(request.POST) # Bind data from request.POST into a PostForm

        # If data is valid, proceeds to create a new post and redirect the user
        if form.is_valid():
            username = form.cleaned_data['username']
            memberfirstname = form.cleaned_data['memberfirstname']
            membermiddlename = form.cleaned_data['membermiddlename']
            memberlastname = form.cleaned_data['memberlastname']
            relation = form.cleaned_data['relation']
            age = form.cleaned_data['age']
            birthdate = form.cleaned_data['birthdate']
            deathdate = form.cleaned_data['deathdate']
            caste = form.cleaned_data['caste']
            religion = form.cleaned_data['religion']
            nationality = form.cleaned_data['nationality']
            occupation = form.cleaned_data['occupation']
            education = form.cleaned_data['education']
            address = form.cleaned_data['address']
            contact = form.cleaned_data['contact']
            familylastname = form.cleaned_data['familylastname']
            uniquename = form.cleaned_data['uniquename']
            person = Person(username=username, memberfirstname=memberfirstname,
                            membermiddlename=membermiddlename, memberlastname=memberlastname, relation=relation,
                            age=age, birthdate=birthdate, deathdate=deathdate, caste=caste, religion=religion,
                            nationality=nationality, occupation=occupation, education=education, address=address,
                            contact=contact, familylastname=familylastname, uniquename=uniquename)
            person.save()
            return HttpResponseRedirect('/home/')
        else:
            data= Person.objects.all()
            messages.error(request, "Required fields must not left blank.")
            return render(request, "mainform.html", {'form':form, 'data': data})

def tree_page(request):  #this is used to filter data from the database for eg: like select query with where condition
    grandfather= Person.objects.filter(relation='grandfather').filter(username=request.user.username)
    grandmother= Person.objects.filter(relation='grandmother').filter(username=request.user.username)
    father= Person.objects.filter(relation='father').filter(username=request.user.username)
    mother= Person.objects.filter(relation='mother').filter(username=request.user.username)
    uncle= Person.objects.filter(relation='uncle').filter(username=request.user.username)
    aunt= Person.objects.filter(relation='aunt').filter(username=request.user.username)
    me= Person.objects.filter(relation='me').filter(username=request.user.username)
    brother= Person.objects.filter(relation='brother').filter(username=request.user.username)
    wife= Person.objects.filter(relation='wife').filter(username=request.user.username)
    unclechild= Person.objects.filter(relation='unclechild').filter(username=request.user.username)
    child= Person.objects.filter(relation='child').filter(username=request.user.username)
    entrylist= Person.objects.filter(username=request.user.username).count()
    return render(request, "familytree.html", {'grandfather': grandfather, 'grandmother': grandmother,
                                               'father': father, 'mother': mother, 'uncle': uncle, 'aunt': aunt,
                                               'me': me, 'brother': brother, 'wife': wife, 'unclechild': unclechild,
                                               'child': child, 'entrylist': entrylist})

def Tree_Main(request, relation): #this is used for filter data according to user defined data
    park = Person.objects.filter(relation=relation).filter(username=request.user.username)
    #treedetail = park.relation
    return render(request, 'familytree.html', {'park': park})

def search_page(request): # this is also used for filter data from database with userdefined input

    familylastname= request.POST['familylastname']
    uniquename= request.POST['uniquename']
    grandfather = Person.objects.filter(relation='grandfather').filter(familylastname=familylastname).filter(uniquename=uniquename)
    grandmother = Person.objects.filter(relation='grandmother').filter(familylastname=familylastname).filter(uniquename=uniquename)
    father = Person.objects.filter(relation='father').filter(familylastname=familylastname).filter(uniquename=uniquename)
    mother = Person.objects.filter(relation='mother').filter(familylastname=familylastname).filter(uniquename=uniquename)
    uncle = Person.objects.filter(relation='uncle').filter(familylastname=familylastname).filter(uniquename=uniquename)
    aunt = Person.objects.filter(relation='aunt').filter(familylastname=familylastname).filter(uniquename=uniquename)
    unclechild = Person.objects.filter(relation='unclechild').filter(familylastname=familylastname).filter(uniquename=uniquename)
    child = Person.objects.filter(relation='child').filter(familylastname=familylastname).filter(uniquename=uniquename)
    me = Person.objects.filter(relation='me').filter(familylastname=familylastname).filter(uniquename=uniquename)
    brother = Person.objects.filter(relation='brother').filter(familylastname=familylastname).filter(uniquename=uniquename)
    wife = Person.objects.filter(relation='wife').filter(familylastname=familylastname).filter(uniquename=uniquename)
    return render(request, "searchresult.html", {'grandfather': grandfather, 'grandmother': grandmother, 'father': father,
                                                 'mother': mother, 'uncle': uncle, 'aunt': aunt, 'me': me, 'brother': brother,
                                                 'wife': wife, 'unclechild': unclechild, 'child': child})

def Search_Main(request, relation, uniquename): #this is also used for filter data according to user defined input
    park = Person.objects.filter(relation=relation).filter(uniquename=uniquename)
    #treedetail = park.relation
    return render(request, 'searchdetail.html', {'park': park})

def Delete_Data(request, relation, id): #this is used to delete data from database
    delt = Person.objects.filter(relation=relation).filter(id=id).filter(username=request.user.username).delete()
    #treedetail = park.relation
    return HttpResponseRedirect('/home/')